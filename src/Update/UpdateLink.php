<?php

namespace Drupal\intlinks\Update;

use Drupal\Core\Url;

class UpdateLink {

    protected $langcode;
    protected $nodes = [];
    protected $languages = [];

    public function __construct($langcode)
    {
      $this->langcode = $langcode;
    }

    public function convertLinks($text)
    {
      return preg_replace_callback('%<a([^>]+?href="([^"]+?)"[^>]*?)>%i', function($matches) {
        return $this->processLink($matches);
      }, $text);
    }

    protected function processLink($matches)
    {
      $parts = parse_url($matches[2]);
      // Path has to be relative.
      // Do nothing if the link is not internal (root rel or doc rel).
      // Do nothing if there is no path, either
      if (!array_key_exists('host', $parts) && !empty($parts['path'])) {
        $parts['path'] = trim($parts['path'], '/');
        if (preg_match('@node/\d+$@', $parts['path'])) {
          $this->populateLanguages();
          $linkWithTitle = $this->getLinkWithTitle($parts['path'], $matches);
          $aliasUrl = $this->buildAliasUrl($parts);
          
          return str_replace($matches[2], $aliasUrl, $linkWithTitle);
        }
      }
      return $matches[0];
    }

    protected function populateLanguages()
    {
      if (empty($this->languages)) {
        $this->languages = \Drupal::languageManager()->getLanguages();
      }
    }

    protected function getLinkWithTitle($path, $matches)
    {
      // If an HTML title attribute already exists...
      if (preg_match('@title="[^"]+?"@', $matches[1])) {
        return $matches[0];
      }

      $nid = str_replace('node/', '', $path);
      if (!isset($this->nodes[$nid])) {
          $this->nodes[$nid] = \Drupal\node\Entity\Node::load($nid);
      }

      return str_replace($matches[1],
          $matches[1] . ' title="' . $this->nodes[$nid]->getTitle() . '"',
          $matches[0]);
    }

    protected function buildAliasUrl($parts, $title)
    {
      $nid = str_replace('node/', '', $parts['path']);
      $params = [
        'node' => $nid
      ];
      $options = [
        'query' => $parts['query'],
        'fragment' => $parts['fragment'],
        'language' => $this->languages[$this->langcode],
        'absolute' => FALSE,
      ];

      return Url::fromRoute('entity.node.canonical', $params, $options)->toString();
    }
}